# QtStudyExamples

## 介绍
Qt学习过程的一些实例及文档。

## 架构
该仓库一共有3个分支：

+ master 分支为主分支
+ develop 为开发过程分支，用于提交学习实践中的过程文件
+ wiki 为wiki文档更新分支，该分支只跟踪wiki目录下的内容

## 使用

### 1. 添加新实例，请选择develop分支克隆

   ```bash
   # 克隆develop分支
   git clone https://gitee.com/tgl233/qt-study-examples.git --branch develop
   # 本地修改
   # 本地提交
   # 推送到服务器(第1个develop指本地分支名,第2个develop指远程分支名)
   git push origin develop:develop
   ```

### 2. 添加或修改wiki文档，请选择wiki分支。并在wiki文件夹下做修改

   ```bash
   # 克隆wiki分支
   git clone https://gitee.com/tgl233/qt-study-examples.git --branch wiki
   # 本地修改
   # 本地提交
   # 推送到服务器(第1个wiki指本地分支名,第2个wiki指远程分支名)
   git push origin wiki:wiki

   # ---------------------------------------------------------------------
   # 添加wiki页面地址
   git remote add wiki-page https://gitee.com/tgl233/qt-study-examples.wiki.git
   # 直接把wiki推送到wiki页面,服务器送必须是master才能在页面上显示出来
   git push wiki-page wiki:master
   ```

### 3. 合并develop分支到master

   ```bash
   # 拉取服务器上master分支
   git fetch origin master:master
   # 切换到master分支
   git checkout master
   # 合并develop分支到master分支上
   git merge --squash develop
   # 提交一次合并
   git commit --all -message "Merge branch 'develop'"   
   ```

## 其它

### 1. 可能会用到的git命令

#### git status 中文名显示为斜线加数字

   ```bash
   $ git status
   On branch wiki
   Your branch is up to date with 'origin/wiki'.

   Changes to be committed:
   (use "git restore --staged <file>..." to unstage)
         new file:   "wiki/\345\244\232\347\272\277\347\250\213_\345\210\233\345\273\272.md"
   ```

   解决方法

   ```bash
   $ git config core.quotePath false
   $ git status
   On branch wiki
   Your branch is up to date with 'origin/wiki'.

   Changes to be committed:
   (use "git restore --staged <file>..." to unstage)
         new file:   wiki/多线程_创建.md
   ```

#### 查看当前分支下的文件列表

   ```bash
   # 查看缓存区文件列表
   git ls-files
   git ls-files --cached
   # 查看被删除的文件
   git ls-files --deleted
   # 查看修改的文件
   git ls-files --modified
   # 查看被忽略的文件
   git ls-files --igignored
   # 查看存储区的文件
   git ls-files --stage
   ```
#### 查看其它分支下的文件列表

   ```bash
   # 查看服务器develop分支下的所有文件列表
   git ls-tree  -r --name-only   --full-tree  origin/develop
   ```

#### 从某个分机下导出某下文件到当前目录下

   ```bash
   # 导出服务器develop分支下的ThreadStartStop/subthread.h到当前目录
   git checkout origin/develop ThreadStartStop/subthread.h
   # 导出服务器develop分支下的ThreadStartStop文件夹下所有文件
   git checkout origin/develop ThreadStartStop
   ```

#### 在当前分支下同步服务器其它分支到本地

   ```bash
   # 同步服务器的wiki分到本地的wiki_local分机上,该命令站在本地创建一个新的wiki_local分支
   git fetch origin wiki:wiki_local
   From https://gitee.com/tgl233/qt-study-examples
      * [new branch]      wiki       -> wiki_local
   ```

#### 创建一个新的孤立的分支

   ```bash
   # 创建一个孤立的分支，创建后的分支没有任务提交记录
   git checkout --orphan new_branch
   # 从当前提交下创建一个新分支，创建后的分支有当前分支的提交记录
   git checkout -b new_branch
   ```

#### 将其它分支合并到本分支下的一个提交
   ```bash
   # 切换到master分支下
   git checkout master
   # 将develop分支下的多次提交合并到master分支上的一次提交
   git merge --squash develop 
   ```

#### 设置服务器地址同时提交多个服务器仓库

   ```bash
   #  给远程origin添加别一个地址
   git remote set-url --add origin  https://gitee.com/tgl233/qt-study-examples.wiki.git
   # 查看添加后的结果
   git remote -v
   origin  https://gitee.com/tgl233/qt-study-examples.git (fetch)
   origin  https://gitee.com/tgl233/qt-study-examples.git (push)
   origin  https://gitee.com/tgl233/qt-study-examples.wiki.git (push)
   ```

#### 将本地分支推送到服务器指定分支上

   ```bash
   # 将本地dev分支推送到服务器develop分支上
   git push origin dev:develop
   # 删除服务器上的test分支
   git push origin :test
   ```

#### 图形化显示所有分支提交

   ```bash
   # 以一行形式图形第显示所有分支的提交信息
   git log --graph --oneline --all
   ```

 ### 2. 代码规范